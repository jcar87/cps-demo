Introduction
============

This project demonstrates how the |CPS| can be used
to provide information about a package
that is suitable for consuming that package.
It uses CMake as the consuming build system.

The |CPS| aims to be a better, tool-agnostic mechanism
for communicating information about a package
in order to allow that package to be consumed.
See https://cps-org.github.io/cps/overview.html for more details.

CMake support for the |CPS| is, as of writing,
very much a work in progress.
The purpose of this repository
is to provide a demonstration of that work.

Overview
========

The premise of CPS is simple:
provide one or more file(s) which describes a package
in sufficient detail to allow that package to be consumed.

These "CPS files" use the ``.cps`` extension,
and serve a similar purpose as pkg-config ``.pc`` files
or CMake ``package-config.cmake`` files.

Sample CPS files are provided for fmt_ and spdlog_,
based on their Fedora 38 packages.
While the demo should work on other platforms,
the CPS files **will** need to be modified.
A ``Dockerfile`` has been provided
to build and run the demo in a suitable environment,
regardless of the host environment.

This demonstration depends on the ``cps-cppcon-demo-2023`` tag
of https://gitlab.kitware.com/matthew-woehlke/cmake.git.
Be aware that this tag has been forced-pushed in the past,
and, if you are reading this prior to CppCon 2023,
is likely to be force-pushed again.
(After CppCon 2023, it will likely be left to bit-rot.)
Note also that there is no expectation of that branch
being merged into CMake in its present form.

Details
=======

In a "normal" ecosystem that fully implements CPS,
the CPS files would be provided by the packages themselves
and would reside in the same location as the other package files
(i.e. headers, compiled libraries).
Additionally, one would simply request the package by name,
e.g. in CMake, using the ``find_package`` command.
This process is also responsible for resolving dependencies.

Because this demonstration was designed
to work with distro-provided packages
and to not require modification of system files,
the CPS files instead live with the demo code,
and are referenced directly
using the *temporary* ``load_package`` command.
This command also expects the package prefix to be provided explicitly,
as the expected relation between the CPS file location
and the location of other package files does not obtain.

In addition, because dependency resolution is not yet implemented,
the ``fmt`` package must be loaded before ``spdlog``.
(There is some provision for using a ``spdlog``
that uses an internal copy of ``fmt``,
but sample CPS files for this are not provided in the repository.)

Functionally, the calls to ``load_package``
are virtually identical to ``find_package``,
particularly as the sample CPS files
are based on the existing CMake package configuration files
that ``fmt`` and ``spdlog`` currently ship.
That is, in CMake, ``load_package`` creates imported targets
which can then be used in exactly the same manner
as the targets created by ``find_package``
from the existing CMake package configuration files.

One notable difference is that CPS has a "default components" concept.
Many packages either provide only a single library,
or (as in the case of fmt and spdlog) have a single target
that is logically the one most consumers will use.
CPS allows specifying which component(s) should be used by default,
which allows consumers to use the package name as a link target,
rather than needing to know specific target names.
This is demonstrated via the ``USE_DEFAULT_COMPONENTS`` option,
which controls whether the demonstration program
is linked to ``spdlog::spdlog`` (an explicit component)
or to ``spdlog`` (using the package's default components).

``load_package``
================

The ``load_package`` command takes two arguments.
The first is an *absolute path* to a CPS file to read.
The second is the value to substitute for ``@prefix@`` in the CPS file(s).
In the future, only ``find_package(spdlog)`` would be necessary.
At present, the full path is given because
a) searching isn't implemented yet, and
b) because CPS files are not included with the packages themselves.
The second, as previously noted,
also relates to why the prefix must be specified.

Result
======

The output of this demonstration project
is a trivial executable which uses spdlog.

The goal of this demonstration is to show that CMake
can read a CPS file and produce usable targets from the same,
and that a consumer can successfully link to those targets.
In theory, CPS files for other packages could be written and consumed,
provided that they do not depend on CPS features not yet implemented.
(Compatibility will of course increase as CPS integration proceeds,
and bug reports or feature requests are of course always welcomed!)

.. ... .. ... .. ... .. ... .. ... .. ... .. ... .. ... .. ... .. ... .. ... ..

.. _fmt: https://github.com/fmtlib/fmt
.. _spdlog: https://github.com/gabime/spdlog

.. |CPS| replace:: Common Package Specification

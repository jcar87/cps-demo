{
  "Cps-Version": "0.8.1",
  "Name": "fmt",
  "Configuration": "release",
  "Components": {
    "fmt": {
      "Location": "@prefix@/lib64/libfmt.so.9.1.0",
      "Link-Name": "libfmt.so.9",
    }
  }
}

{
  "Cps-Version": "0.8.1",
  "Name": "fmt",
  "Version": "10.1.1",
  "Compat-Version": "10.0.0",
  "Platform": {
    "Isa": "arm64",
    "Kernel": "darwin",
    "C-Runtime-Vendor": "apple",
    "C-Runtime-Version": 13.1
  },
  "Default-Components": [
    "fmt"
  ],
  "Configurations": [
    "release",
    "debug"
  ],
  "Components": {
    "fmt": {
      "Type": "dylib",
      "Compile-Features": [
        "c++11"
      ],
      "Definitions": [
        "FMT_SHARED"
      ],
      "Includes": [
        "@prefix@/include"
      ]
    },
    "fmt-header-only": {
      "Type": "interface",
      "Compile-Features": [
        "c++11"
      ],
      "Definitions": [
        "FMT_HEADER_ONLY=1"
      ],
      "Includes": [
        "@prefix@/include"
      ]
    }
  }
}

{
  "Cps-Version": "0.8.1",
  "Name": "fmt",
  "Configuration": "release",
  "Components": {
    "fmt": {
      "Location": "@prefix@/lib/libfmt.10.1.0.dylib",
      "Link-Name": "@rpath/libfmt.10.dylib"
    }
  }
}
